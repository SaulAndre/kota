import React, { Component } from 'react'

export default class CartItem extends Component {
    render() {
        return (
            <div className="border rounded shadow-sm d-flex p-3 mb-3">
                <div className="img mr-3">
                    <img src={this.props.item.img} 
                    style={{ width: "5rem"}}/>
                </div>
                <div className="info">
                    <p className="text-weight-bold m-0">{this.props.item.name}</p>
                    <p className="text-danger font-weight-bold">Rp. {this.props.item.price}</p>
                </div>
            </div>
                
        )
    }
}
