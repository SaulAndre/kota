import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

export default class Product extends Component {
    render() {
        const { products, searched } = this.props;
        const productList = products.filter(product => product.name.toLowerCase().includes(this.props.searched.toLowerCase())).map(filteredProduct => {
            return (
                <Card className="p-1 mr-2 mb-4 shadow-sm" style={{ width: '12rem', height: '23rem' }} key={filteredProduct.id}>
                    <Card.Img variant="top" src={filteredProduct.img} />
                    <Card.Body className="p-2 d-flex flex-column">
                        <Card.Title className="flex-grow-1">{filteredProduct.name}</Card.Title>
                        <Card.Text className="text-danger font-weight-bold">
                        Rp. {filteredProduct.price}
                        </Card.Text>
                    </Card.Body>
                    <Button variant="primary" type="submit" size="sm" className="" 
                        onClick={() => this.props.onClick(filteredProduct)}>
                        Add to cart
                    </Button>
                </Card>
            )
        })

        return (
            <div className="product-list d-flex flex-wrap">
                { productList }
            </div>
        )
    }
}
