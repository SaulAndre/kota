import React, { Component } from 'react'
import Navbar from "react-bootstrap/Navbar"

export default class Header extends Component {
	handleClick = (e) => {
	}
	render() {
		return (
			<Navbar className="mb-2 sticky-top" bg="dark" variant="dark">
				<Navbar.Brand href="/">
					<img
						alt=""
						src="/logo.svg"
						width="30"
						height="30"
						className="d-inline-block align-top"
					/>{' '}
					E-Commerce
				</Navbar.Brand>
				<Navbar.Toggle />
					<Navbar.Collapse className="justify-content-end">
						<Navbar.Text>
						<a href="/checkout" onClick={this.handleClick}>Cart <span className="p-1 bg-danger text-white rounded">{this.props.numbOfCart}</span></a>
						</Navbar.Text>
					</Navbar.Collapse>
			</Navbar>
		)
	}
}
