import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

export default class AddProduct extends Component {
    state = {
        img: null,
        name: null,
        price: null
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addProduct(this.state)
    }
    render() {
        return (
            <Card className="p-2 mr-2 mb-4 shadow-sm bg-light" 
            style={{ width: '12rem', height: '23rem', cursor: "pointer"}}
            >
                <h5>Add Product</h5>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group>
                        <Form.Control id="img" className="mb-2" type="text" placeholder="image source" onChange={this.handleChange}/>
                        <Form.Control id="name" className="mb-2" type="text" placeholder="product name" onChange={this.handleChange}/>
                        <Form.Control id="price" className="mb-2" type="text" placeholder="price" onChange={this.handleChange}/>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Add Product
                    </Button>
                </Form>
            </Card>
        )
    }
}
