import React, { Component } from 'react'
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"
import Header from "../../Component/Header/Header"
import Product from "../../Component/Product/Product"
import AddProduct from "../../Component/AddProduct/AddProduct"

export default class Home extends Component {
    state = {
        products: [
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2018/11/28/14100407/14100407_1c1884b9-288f-47d7-bfbf-5f2225b5fb0e_840_840.jpg",
                name: "Razer Nari Ultimate",
                price: 3765000,
                id: 1
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/4/18/3932984/3932984_7675228f-7dee-49f3-9684-67a0d449919a_1000_1000",
                name: "Razer Nari Essensial",
                price: 1800000,
                id: 2
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/4/21/14100407/14100407_277e9325-8226-48ba-92c6-9e003a8791ce_1000_1000",
                name: "Razer Goliathus Mobile",
                price: 255000,
                id: 3
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/5/19/19624670/19624670_683aa1fa-1457-409b-8d47-329185c3b175_1000_1000",
                name: "Razer Kraken Stormtrooper Edition",
                price: 1700000,
                id: 4
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/5/19/19624670/19624670_171259b0-6d5d-4754-87b9-f15e8d025a45_1000_1000",
                name: "Razer Kraken Console - Free L33T Pack",
                price: 1310000,
                id: 5
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/5/19/19624670/19624670_3da2bde8-8488-4c00-8852-80c5b320b824_1000_1000",
                name: "Razer Kraken X - Mercury Edition",
                price: 1065000,
                id: 6
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/5/20/3393118/3393118_ac5f987f-9cac-4340-a132-153d68adecab_1040_1040",
                name: "Razer Cynosa Lite",
                price: 850000,
                id: 7
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/6/28/17591745/17591745_9d547c3e-310e-4616-83b3-08de365841f8_800_800",
                name: "Razer Mouse Bungee V2",
                price: 460000,
                id: 8
            }
        ],
        cart: [
        ],
        searched: ""
    }
    addToCart = (product) => {
        var tempCart = [...this.state.cart, product]
        this.setState({
            cart: tempCart
        }, function() {
            console.log(this.state.cart)
        })
    }
    addProduct = (product) => {
        product.id = Math.floor(Math.random() * 100) + 1;
        var tempProducts = [...this.state.products, product]
        this.setState({
            products: tempProducts
        })
    }

    handleChange = (e) => {
        this.setState({
            searched: e.target.value
        })
    }

    render() {
        return (
            <div className="home">
                <Row>
                    <Col>
                        <Header numbOfCart={this.state.cart.length}/>
                    </Col>
                </Row>
                <Row className="ml-3 mr-3 mb-3">
                    <Col xs={8}>
                        <Form className="d-flex">
                            <Form.Group className="m-0 flex-grow-1" controlId="formBasicEmail">
                                <Form.Control placeholder="find product" onChange={this.handleChange}/>
                            </Form.Group>
                            <Button variant="primary" type="submit" size="sm" className="ml-2">
                                Submit
                            </Button>
                        </Form>
                    </Col>
                </Row>
                <Row className="ml-3 mr-3">
                    <Col >
                        <Product products={this.state.products} onClick={this.addToCart} searched={this.state.searched}/>
                        <AddProduct addProduct={this.addProduct} />
                    </Col>
                </Row>
            </div>
        )
    }
}
