import React, { Component } from 'react'
import {Row, Col, Button} from "react-bootstrap"
import Header from "../../Component/Header/Header"
import CartItem from "../../Component/CartItem/CartItem"

export default class Checkout extends Component {
	state = {
		cart: [
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2018/11/28/14100407/14100407_1c1884b9-288f-47d7-bfbf-5f2225b5fb0e_840_840.jpg",
                name: "Razer Nari Ultimate",
                price: 3765000,
                id: 1
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/4/18/3932984/3932984_7675228f-7dee-49f3-9684-67a0d449919a_1000_1000",
                name: "Razer Nari Essensial",
                price: 1800000,
                id: 2
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/4/21/14100407/14100407_277e9325-8226-48ba-92c6-9e003a8791ce_1000_1000",
                name: "Razer Goliathus Mobile",
                price: 255000,
                id: 3
            },
            {
                img: "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/5/19/19624670/19624670_683aa1fa-1457-409b-8d47-329185c3b175_1000_1000",
                name: "Razer Kraken Stormtrooper Edition",
                price: 1700000,
                id: 4
            }
        ]
    }
    render() {
        var totalPrice = 0;
        const cartList = this.state.cart.map(item => {
            totalPrice = totalPrice + parseInt(item.price)
            return (
                <CartItem item={item} key={item.id}/>
                )
            })
        return (
            <div className="checkout">
                <Row>
                    <Col>
                        <Header numbOfCart={0} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h4 className="h-1 mb-3 p-3 border-bottom">Cart List</h4>
                    </Col>
                </Row>
                <Row className="cart-list ml-3 mr-3 mb-3">
                    <Col md={8} >
                        { cartList }
                    </Col>
                    <Col md={4} >
                        <div className="price d-flex flex-column border shadow-sm p-3 rounded">
                            <h5>Price</h5>
                            <div className="total d-flex ">
                                <p className="flex-grow-1 ">Total:</p>
                                <p className="text-danger">Rp. {totalPrice}</p>
                            </div>
                            <Button>Proceed</Button>
                        </div>
                    </Col>
                </Row>
                <Row className="total-price ml-3 mr-3">
                    <Col>
                    </Col>
                </Row>
                <Row className="payment">
                    <Col>

                    </Col>
                </Row>
            </div>
        )
    }
}
