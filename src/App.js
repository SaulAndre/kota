import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom"
import { Container } from 'react-bootstrap';
import Home from "./Pages/Home/Home"
import Checkout from "./Pages/Checkout/Checkout"
import ProductDetail from "./Pages/ProductDetail/ProductDetail"
import 'bootstrap/dist/css/bootstrap.min.css';
import "./App.css"

function App() {
  return (
    <Container fluid className="p-0">
      <Router>
        <Switch>
          <Route path="/checkout" component = {Checkout} />
          <Route path="/" component = {Home} />
        </Switch>
      </Router>
    </Container>
  );
}

export default App;
